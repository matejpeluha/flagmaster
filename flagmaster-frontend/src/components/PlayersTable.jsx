import React, {Component} from 'react';

import "../css-components/players-table.css"

class PlayersTable extends Component {
    render() {
        return (
            <table>
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Hráč
                        </th>
                        <th>
                            Body
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.allPlayers.map((player, key) => this.getTableRow(player, key))}
                </tbody>
            </table>
        );
    }

    getTableRow = (player, key) =>{
        return <tr key={key}><td>{player.id}</td><td>{player.nickname}</td><td>{player.points}</td></tr>;
    }
}


export default PlayersTable;