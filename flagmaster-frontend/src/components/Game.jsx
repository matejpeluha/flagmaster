import React, {Component} from 'react';
import PlayersTable from "./PlayersTable";
import WaitingView from "./WaitingView";

import "../App.css"
import GameView from "./GameView";

class Game extends Component {
    render() {
        return (
            <div id={this.props.rootId}>
                <h2>{"ID:" + this.props.playerId}</h2>
                <h2>{this.props.nickname}</h2>


                <WaitingView rootId={"waiting-view"}
                             playing={this.props.playing}
                             allPlayers={this.props.allPlayers}
                             onGameStarted={this.props.onGameStarted}
                />

                <GameView rootId={"game-view"}
                          imageSource={this.props.imageSource}
                          answers={this.props.answers}
                          playing={this.props.playing}
                          buttonsDisabled={this.props.buttonsDisabled}
                          lastQuestionInfo={this.props.lastQuestionInfo}
                          questionNumber={this.props.questionNumber}
                          onAnswered={this.props.onAnswered}
                          onEndGame={this.props.onEndGame}
                          allPlayers={this.props.allPlayers}
                />

            </div>
        );
    }
}


export default Game;