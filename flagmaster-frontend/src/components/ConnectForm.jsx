import React, {Component} from 'react'

import "../css-components/connect-form.css"
import "../App.css";

class ConnectForm extends Component {
    render() {
        return (
            <div id={"form-container"}>

                <form id={this.props.rootId}  onSubmit={(event) => this.props.onSubmit(event)}>
                    <h1 id={"header-flagmaster"}>Flagmaster</h1>
                    <input id={"nickname-input"} type={"text"} placeholder={"Prezývka"} required/><br/>
                    <button type={"submit"} className={"action-button"}>Pripojiť sa</button>
                </form>
            </div>
        );
    }


}

export default ConnectForm;
