import React, {Component} from 'react';

import "../css-components/game-view.css"
import "../App.css"
import PlayersTable from "./PlayersTable";

class GameView extends Component {
    render() {
        return (
            <div id={this.props.rootId} className={this.getVisibility()}>
                <div id={"end-game"} className={this.getVisibilityOfEnding()}>
                    <h2>Koniec hry </h2>
                    <button onClick={this.props.onEndGame} className={"action-button"}>Ukončiť</button>
                    <PlayersTable allPlayers={this.props.allPlayers}/>
                </div>

                <div id={"last-question-info"} className={this.getVisibilityOfLastQuestionInfo()}>
                    <div>
                        <h3>Správna odpoveď: </h3>
                        <p>{this.props.lastQuestionInfo.lastRightAnswer}</p>
                        <h3>Body získal: </h3>
                        <p>{this.props.lastQuestionInfo.lastRightName}</p>
                    </div>
                </div>

                <div id={"game-question"} className={this.getVisibilityOfQuestion()}>
                    <h4>{"Otázka: " + this.props.questionNumber + "/20"}</h4>
                    <img id={"flag-img"} src={process.env.PUBLIC_URL + "/images/" + this.props.imageSource} alt="vlajka" />
                    <br/>
                    <button onClick={this.handleOnAnswered} className={"action-button " + this.getActivityStateButton()}
                            disabled={this.props.buttonsDisabled}>
                        {this.props.answers[0]}
                    </button>
                    <button onClick={this.handleOnAnswered} className={"action-button " + this.getActivityStateButton()}
                            disabled={this.props.buttonsDisabled}>
                        {this.props.answers[1]}
                    </button>
                    <button onClick={this.handleOnAnswered} className={"action-button " + this.getActivityStateButton()}
                            disabled={this.props.buttonsDisabled}>
                        {this.props.answers[2]}
                    </button>
                    <button onClick={this.handleOnAnswered} className={"action-button " + this.getActivityStateButton()}
                            disabled={this.props.buttonsDisabled}>
                        {this.props.answers[3]}
                    </button>
                </div>

            </div>
        );
    }

    handleOnAnswered = (event) =>{
        const button = event.target;

        this.props.onAnswered(button.innerText);
    }

    getVisibility(){
        if(this.props.playing)
            return "visible";

        return "hidden";
    }

    getVisibilityOfEnding(){
        if(this.props.playing && this.props.imageSource === "" && !this.props.lastQuestionInfo.showTime)
            return "visible";

        return "hidden";
    }

    getVisibilityOfQuestion(){
        if(this.props.playing && this.props.imageSource !== "" && !this.props.lastQuestionInfo.showTime)
            return "visible";

        return "hidden";
    }

    getActivityStateButton(){
        if(this.props.buttonsDisabled)
            return "disabled-button";

        return "0";
    }

    getVisibilityOfLastQuestionInfo(){
        if(this.props.lastQuestionInfo.showTime)
            return "visible"

        return "hidden";
    }
}

export default GameView;