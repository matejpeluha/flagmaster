import React, {Component} from 'react';
import { w3cwebsocket as W3CWebSocket } from "websocket";

import "../css-components/front-page.css"
import ConnectForm from "./ConnectForm";
import Game from "./Game";

let SOCKET;

class FrontPage extends Component {

    state = {
        player: {
            id: 0,
            nickname: "",
            points: 0
        },
        allPlayers: [],
        playing: false,
        flag: "",
        answers: ["", "", "", ""],
        buttonsDisabled: false,
        questionNumber: 1,
        lastQuestionInfo: {
            lastRightAnswer: "",
            lastRightName: "",
            showTime: false
        }

    }


    render() {
        return (
            <main id={"window"}>
                <ConnectForm rootId={"connect-form"}  onSubmit={this.handleSubmitNickname}/>
                <Game rootId={"game"}
                      allPlayers={this.state.allPlayers}
                      playerId={this.state.player.id}
                      nickname={this.state.player.nickname}
                      playing={this.state.playing}
                      imageSource={this.state.flag}
                      answers={this.state.answers}
                      buttonsDisabled={this.state.buttonsDisabled}
                      lastQuestionInfo={this.state.lastQuestionInfo}
                      questionNumber={this.state.questionNumber}
                      onGameStarted={this.handleGameStarted}
                      onAnswered={this.handleOnAnswered}
                      onEndGame={this.handleOnEndgame}
                />
            </main>
        );
    }

    handleSubmitNickname = (event) =>{
        event.preventDefault()

        const nicknameInput = document.getElementById("nickname-input");
        const nickname = nicknameInput.value;

        this.setState({
            player: {
                id: 0, nickname: nickname, point: 0
            },
            allPlayers: this.state.allPlayers,
            playing: this.state.playing,
            flag: this.state.flag,
            answers: this.state.answers,
            buttonsDisabled: this.state.buttonsDisabled,
            questionNumber: this.state.questionNumber,
            lastQuestionInfo: this.state.lastQuestionInfo
        });

        this.openGame();

        SOCKET = new W3CWebSocket("wss://wt118.fei.stuba.sk:9000");
        SOCKET.onmessage = this.handleSocketMessage;
    }

    openGame(){
        document.getElementById("form-container").style.display = "none";
        document.getElementById("game").style.display = "block";

        window.addEventListener("beforeunload", (ev) =>
        {
            ev.preventDefault();
            this.sendPlayer("delete-player-request");
        });
    }

    handleGameStarted = () =>{
        this.sendPlayer("start-game-request");
    }

    handleOnAnswered = (answer) =>{
        this.setState({
            player: this.state.player,
            allPlayers: this.state.allPlayers,
            playing: this.state.playing,
            flag: this.state.flag,
            answers: this.state.answers,
            buttonsDisabled: true,
            questionNumber: this.state.questionNumber,
            lastQuestionInfo: this.state.lastQuestionInfo
        });


        const info = {request: "answer-request", player: this.state.player, answer: answer, flag: this.state.flag};
        SOCKET.send(JSON.stringify(info));
    }


    handleOnEndgame = () =>{
        const info = {request: "end-game-request"};
        SOCKET.send(JSON.stringify(info));
    }



    handleSocketMessage = event =>{
        const json = JSON.parse(event.data)

        if(json.request === "player-request"){
            this.onPlayerRequest(json);
        }
        else if(json.request === "update-request"){
            this.onUpdateRequest(json);
        }
        else if(json.request === "next-question-request"){
            this.onNextQuestionRequest(json);
        }
        else if(json.request === "game-is-finished-request"){
            this.onGameIsFinishedRequest();
        }

    }

    onPlayerRequest(json){
        const id = this.getNewId(json.allPlayers);
        this.setState({
            player:
                {
                    id: id,
                    nickname: this.state.player.nickname,
                    point: 0
                },
            allPlayers: this.state.allPlayers,
            playing: json.playing,
            flag: json.question.flag,
            answers: json.question.allAnswers,
            buttonsDisabled: this.state.buttonsDisabled,
            questionNumber: this.state.questionNumber,
            lastQuestionInfo: this.state.lastQuestionInfo
        }, () => this.sendPlayer("add-player-request"));
    }

    onUpdateRequest(json){
        let flag = "";
        let answers = ["", "", "", ""];
        let questionNumber = 1;

        if(json.question){
            flag = json.question.flag;
            answers = json.question.allAnswers;
            questionNumber = json.questionNumber;
        }

        this.setState({
            player: this.state.player,
            allPlayers: json.allPlayers,
            playing: json.playing,
            flag: flag,
            answers: answers,
            buttonsDisabled: this.state.buttonsDisabled,
            questionNumber: questionNumber,
            lastQuestionInfo: this.state.lastQuestionInfo
        });
    }


    onNextQuestionRequest(json){
        this.setState({
            player: this.state.player,
            allPlayers: this.state.allPlayers,
            playing: this.state.playing,
            flag: this.state.flag,
            answers: this.state.answers,
            buttonsDisabled: false,
            questionNumber: this.state.questionNumber,
            lastQuestionInfo: {
                lastRightAnswer: json.lastAnswer,
                lastRightName: json.playerName,
                showTime: true
            }
        }, this.showNextQuestionAfterTimeOut);
    }

    showNextQuestionAfterTimeOut = () =>{
        setTimeout(this.showNextQuestion, 3000);

    }

    showNextQuestion = () =>{
        this.setState({
            player: this.state.player,
            allPlayers: this.state.allPlayers,
            playing: this.state.playing,
            flag: this.state.flag,
            answers: this.state.answers,
            buttonsDisabled: false,
            questionNumber: this.state.questionNumber,
            lastQuestionInfo: {
                lastRightAnswer: this.state.lastQuestionInfo.lastRightAnswer,
                lastRightName: this.state.lastQuestionInfo.lastRightName,
                showTime: false
            }
        });
    }

    onGameIsFinishedRequest(){
        this.setState({
            player: {
                id: 0,
                nickname: "",
                points: 0
            },
            allPlayers: [],
            playing: false,
            flag: "",
            answers: ["", "", "", ""],
            buttonsDisabled: false,
            questionNumber: this.state.questionNumber,
            lastQuestionInfo: this.state.lastQuestionInfo
        }, () => window.location.reload());
    }




    getNewId(allPlayers){
        let id = 1;

        const allPlayersLength = allPlayers.length;
        if(allPlayersLength < 1)
            return id;

        const lastId = allPlayers[allPlayersLength - 1].id;

        id = lastId + 1;

        return id;
    }

    sendPlayer = (request) =>{
        const info = {request: request, player: this.state.player};
        SOCKET.send(JSON.stringify(info));
    }

}

export default FrontPage;
