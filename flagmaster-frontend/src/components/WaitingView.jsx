import React, {Component} from 'react';

import "../css-components/waiting-view.css"
import "../App.css"
import PlayersTable from "./PlayersTable";

class WaitingView extends Component {
    render() {
        return (
            <div id={this.props.rootId} className={this.getVisibility()}>
                <p>
                    Keď budú všetci hráči pripravení, odštartuje hru.
                </p>
                <button onClick={this.props.onGameStarted} className={"action-button"}>Štart</button>

                <PlayersTable allPlayers={this.props.allPlayers}/>
            </div>
        );
    }

    getVisibility(){
        if(this.props.playing)
            return "hidden";

        return "visible"
    }
}

export default WaitingView;