import React, {Component} from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import FrontPage from "./components/FrontPage";

import './App.css';



class App extends Component{


    render() {
      return (
              <Router basename={"/flagmaster"}>

                  <lottie-player id={"background-lottie"}
                                 src="https://assets6.lottiefiles.com/packages/lf20_b1imuadj.json"
                                 speed="1" loop  autoplay/>

                  <Switch>
                      <Route path={"/"} exact component={FrontPage}/>
                  </Switch>

              </Router>
      );
    }



}

export default App;
