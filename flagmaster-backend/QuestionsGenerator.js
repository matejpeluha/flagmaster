module.exports = class QuestionsGenerator{

    constructor() {
        this.allFlags = [];
        this.chosenFlags = [];
        this.questions = [];
    }


    generateQuestions(questionsCount){
        this.loadJson();
        this.fillChosenFlags(questionsCount);
        this.createQuestions();


        return this.questions;
    }

    loadJson(){
        const fs = require('fs');

        let rawdata = fs.readFileSync('countries.json');
        this.allFlags = JSON.parse(rawdata).countries;
    }

    fillChosenFlags(questionCount){
        while(this.chosenFlags.length < questionCount){
            const index = Math.floor(Math.random() * 69) ;
            const flag = this.allFlags[index];

            if(this.chosenFlags.indexOf(flag) === -1) {
                this.chosenFlags.push(flag);
            }
        }
    }

    createQuestions(){
        for(let flag of this.chosenFlags){
            this.pushQuestion(flag);
        }
    }

    pushQuestion(flag){
        const answers = this.getAnswersFor(flag.country);

        const question = {
            flag: flag.flag,
            rightAnswer: flag.country,
            allAnswers: answers
        }

        this.questions.push(question);
    }

    getAnswersFor(country){
        const answers = this.getRandomAnswers();

        if(answers.indexOf(country) === -1) {
            const index = Math.floor(Math.random() * 4);
            answers[index] = country;
        }

        return answers;
    }

    getRandomAnswers(){
        const randomAnswers = [];
        while(randomAnswers.length < 4){
            const index = Math.floor(Math.random() * 69) ;
            const flag = this.allFlags[index];

            if(randomAnswers.indexOf(flag.country) === -1) {
                randomAnswers.push(flag.country);
            }
        }

        return randomAnswers;
    }
}
