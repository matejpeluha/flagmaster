const QS = require("./QuestionsGenerator");

const WebSocket = require('ws');
const https = require('https');
const fs = require('fs');
const Mutex = require('async-mutex').Mutex;
const mutex = new Mutex();

const maxQuestionsCount = 20;
let allPlayers = [];
let playing = false;
let allQuestions = [];
let question = {
    flag: "",
    rightAnswer: "",
    allAnswers: ["", "", "", ""],
}
let lastQuestion = question;
let lastRightPlayerName = "";

let answersCount = 0;

const server = https.createServer({
    cert: fs.readFileSync("/etc/letsencrypt/live/wt118.fei.stuba.sk/fullchain.pem"),
    key: fs.readFileSync("/etc/letsencrypt/live/wt118.fei.stuba.sk/privkey.pem")
});

server.listen(9000);

const ws = new WebSocket.Server({server});

ws.on('connection', (socket, req) => {
    console.log("Connection from", req.connection.remoteAddress);

    socket.on("message", onSocketMessage);

    const info = {request: "player-request", allPlayers: allPlayers, playing: playing, question: question};
    socket.send(JSON.stringify(info));

})


onSocketMessage = (message) =>{
    const parsedJson = JSON.parse(message);
    const request = parsedJson.request;

    if(request === "add-player-request")
        onAddPlayerRequest(parsedJson);
    else if(request === "delete-player-request")
        onDeletePlayerRequest(parsedJson);
    else if(request === "start-game-request")
        onStartGameRequest();
    else if(request === "answer-request")
        onAnswerRequest(parsedJson);
    else if(request === "end-game-request")
        onEndGameRequest();

}

function onEndGameRequest(){
    playing = false;
    question = {
        flag: "",
        rightAnswer: "",
        allAnswers: [],
    }
    lastQuestion = question;
    allQuestions = [];
    allPlayers = [];
    lastRightPlayerName = "";

    console.log("konec hry");

    ws.clients.forEach(function each(client)  {
        if(client.readyState === WebSocket.OPEN){
            const info = {request: "game-is-finished-request", allPlayers: allPlayers, playing: playing};
            client.send(JSON.stringify(info));
        }
    })
}

function onAnswerRequest(json){
    const player = json.player;
    const answer = json.answer;
    const flag = json.flag;

    answersCount++;

    if (answer === question.rightAnswer && flag === question.flag ) {
        mutex.runExclusive(function () {
            if (answer === question.rightAnswer && flag === question.flag) {
                const playerIndex = getPlayerIndex(player);
                allPlayers[playerIndex].points += 50;
                lastRightPlayerName = player.nickname;
                lastQuestion = question;
                question = giveNextQuestion();
                sendRequestsAfterQuestion();
            }
        }).then(r => console.log("answer " + answer + " passed"))
    }
    else if(answersCount === allPlayers.length){
        lastRightPlayerName = "";
        lastQuestion = question;
        question = allQuestions.pop();
        sendRequestsAfterQuestion();
    }
}

function giveNextQuestion(){
    let newQuestion = allQuestions.pop();

    if(!newQuestion){
        newQuestion = {
            flag: "",
            rightAnswer: "",
            allAnswers: [],
        }
    }

    return newQuestion;
}

function sendRequestsAfterQuestion(){
    answersCount = 0;
    const questionNumber = maxQuestionsCount - allQuestions.length;

    ws.clients.forEach(function each(client)  {
        if(client.readyState === WebSocket.OPEN){

            const info1 = {request: "next-question-request", lastAnswer: lastQuestion.rightAnswer, playerName: lastRightPlayerName};
            const info2 = {request: "update-request", allPlayers: allPlayers, playing: playing, question: question, questionNumber: questionNumber};

            client.send(JSON.stringify(info1));
            client.send(JSON.stringify(info2));
        }
    })
}

function onStartGameRequest(){
    if(playing)
        return;

    playing = true;
    const questionsGenerator = new QS();
    allQuestions = questionsGenerator.generateQuestions(maxQuestionsCount);


    //console.log(allQuestions);
   question = allQuestions.pop();

    sendUpdateRequestToAll();
}


function onDeletePlayerRequest(json){
    const player = json.player;

    if(player.nickname === "")
        return;

    console.log("player " + player.nickname + " deleted");

    const playerIndex = getPlayerIndex(player);

    if(playerIndex >= 0)
        allPlayers.splice(playerIndex, 1);

    if(allPlayers.length > 0)
        sendUpdateRequestToAll();
    else
        onEndGameRequest();
}

function onAddPlayerRequest(json){
    const player = json.player;
    const nickname = player.nickname;
    const id = player.id;

    allPlayers.push({nickname: nickname, id: id, points: 0});

   sendUpdateRequestToAll();

}

function sendUpdateRequestToAll(){
    const questionNumber = maxQuestionsCount - allQuestions.length;

    ws.clients.forEach(function each(client)  {
        if(client.readyState === WebSocket.OPEN){
            const info = {request: "update-request", allPlayers: allPlayers, playing: playing, question: question, questionNumber: questionNumber};
            client.send(JSON.stringify(info));
        }
    })
}


function getPlayerIndex(player){
    for(let index = 0; index < allPlayers.length; index++){
        if(allPlayers[index].id === player.id && allPlayers[index].nickname === player.nickname){
            return index;
        }
    }

    return -1;
}

console.log("Server is running...");